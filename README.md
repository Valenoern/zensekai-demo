```
           .. .,
          |_\|_/|
   ______/     `\/|
  \      ＼(u)    /      ___
   ＼/\___／    _/     /_    ＼
     `-` \     / ／＼    ＼^^^＼
     ___/      \`~`~`\_    ＼> |
     \          `~`~`  ＼  ／`／
     \_＼.／   ,--,--,__ \|__|
       ＼^_- ／-＼ /vVVv\||  |
       |.-|~~|   ＼|vVVVv\|  |
      .-.--.~|    .---.__./  /
     |__|____/   |________|／

```

ASEKAI, a libre [mon](https://allthetropes.org/wiki/Mon) simulation game...

...makes its home on **codeberg.org**.  
There are many other places to host your git repositories than github or gitlab, many now using Free Software and more privacy/anonymity friendly.  
Gitlab is certainly no github, but there is still a case for moving.

https://codeberg.org/Valenoern/zensekai-demo
