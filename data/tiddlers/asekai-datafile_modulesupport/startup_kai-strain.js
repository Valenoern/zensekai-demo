/*\
jedit: :folding=explicit: {{{
created: 20190320004208363
tags: $:/asekai/datafile/kai-strain
title: $:/asekai/datafile/startup/kai-strain.js
type: application/javascript
module-type: startup

kai strain plugin support

\*/
(function(){
/*jslint node: true, browser: true, esnext: true */
/*global $tw: false */
"use strict";

var
Hjson = require('$:/plugins/laktak/hjson/hjson.js')
, heart = require('$:/asekai/datafile/lib/heart.js')
, util = require('$:/asekai/datafile/util.js').util;

// Export name and synchronous status
// $:/core/modules/startup/kai-strain.js
exports.name = "kai-strains";
exports.before = [ "startup" ];
exports.after = [ "load-modules" ];
exports.synchronous = true;
// }}}


var getKaiPlugins = function(pluginType) { // {{{
	var plugin, configTid, configTidExp,
		registeredTitles = [], i;
	const wiki = $tw.wiki;
		
	// get name of current language
	if (pluginType == 'kai-lang') {
		configTid = wiki.getTextReference('$:/asekai/config/current-language!!text');
		configTidExp = new RegExp('^\\$\\:\/asekai\/language\/' + configTid + '\/.+');
	}
	
	wiki.each(function(tiddler,title) {
		var disablingTiddler;
			
		// get plugin if it's not disabled
		if(tiddler && tiddler.fields["plugin-type"] === pluginType) {
			// but for language tids you only want one so maybe disable it
			if (pluginType == 'kai-lang') {
				wiki.deleteTiddler("$:/config/Plugins/Disabled/" + title);
				
				if (configTidExp.test(title) === true) {
					disablingTiddler = 'no';
				}
				else {
					// LATER: introduce a sophisticated way to figure out
					// what tids are missing from translations
					// instead of leaving whole english plugin
					if (title == '$:/asekai/lang/en/basic') {
						disablingTiddler = 'no';
					}
					else {
						disablingTiddler = 'yes';
					}
				}
				
				wiki.addTiddler({
					title: "$:/config/Plugins/Disabled/" + title,
					text: disablingTiddler
				});
				
				if (disablingTiddler === 'yes') {
					return;
				}
			}
			else {
				disablingTiddler = wiki.getTextReference("$:/config/Plugins/Disabled/" + title + '!!text');
			}
			
			if (disablingTiddler !== "yes") {
				plugin = {};
				
				for (i in tiddler.fields) {
					plugin[i] = tiddler.fields[i];
				}
				
				registeredTitles.push(plugin);
			}
		}
	});

	return registeredTitles;
}; // }}}

/*exports.unabbreviate = function(plugin) {
	// unpack 'dir', 'strain' structure: 
	for (i in plugin) {
		// for each '_DIR'
		if (typeof plugin[i] == 'object' && plugin[i]._DIR == 'dir') {
			prefix = '$:/asekai/lang/'+ la +'/' + i;
			delete plugin[i]._DIR;
			
			// for each 'strain'
			for (j in plugin[i]) {
				if (typeof plugin[i][j] == 'object' && plugin[i][j]._DIR == 'strain') {	
					delete plugin[i][j]._DIR;
					
					// for each form
					for (k in plugin[i][j]) {
						prefix2 = prefix + '/' + j + '_';
							
						plugin[prefix2 + k] = plugin[i][j][k];
					}
				}
			}
			
			delete plugin[i];
		}
	}
	//
};*/

// unabbreviate structures like
// k_feuvog: { feuvog: {} }  ->  k_feuvog_feuvog: {}
exports.unAbbr = function(plugin) { // {{{
	var i, j, separator;
	
	for (i in plugin) {
		if (typeof plugin[i] == 'object' && /^(yes|strain)$/.test(plugin[i]._DIR)) {
			separator = '';
			
			if (plugin[i]._DIR == 'strain') {
				separator = '_';
			}
			
			delete plugin[i]._DIR;
			
			for (j in plugin[i]) {
				plugin[i + separator + j] = plugin[i][j];
			}
			
			delete plugin[i];
		}
	}
	
	return plugin;
}; // }}}

// process a strainwide data tid
var strainMainTid = function(maintid, strainIndex) { // {{{
	var strainName;
	
	strainName = maintid['num-ident'];
	
	maintid['asekai-datatype'] = 'k-strain';
	maintid.tags = '[[Kai Book]]';
	
	if (maintid.number !== undefined && typeof maintid.number == 'object') {
		maintid['num-sector'] = maintid.number.q;
		maintid['num-index'] = maintid.number.i;
		maintid['num-serial'] = maintid.number.s;
		maintid['num-rarity'] = maintid.number.r;
		
		delete maintid.number;
	}
	
	// if numbers are in an index instead of in strain, pick it up
	// for now, sector number is left a number on strains - though ident on forms
	// later, it may be changed to ident, & sectors carefully sorted by sphere
	// (so you can have a sensible kai guide with even 5 spheres worth of mods)
	if ( strainIndex.strains[strainName] !== undefined && maintid['num-index'] === undefined ) {
		// some kai (usually badsectors) can have multiple numbers.
		// if they have a sector defined, get their number from THAT sector
		if (maintid['num-sector'] !== undefined) {
			maintid['num-sector'] = strainIndex.codes[ maintid['num-sector'] ];
			maintid['num-index'] =
			strainIndex.strains[strainName].sectors[ maintid['num-sector'] ];
		}
		else {
			maintid['num-sector'] = strainIndex.strains[strainName].q;
			maintid['num-index'] = strainIndex.strains[strainName].i;
		}
	}
	
	// rarity tags
	// if num-rarity isn't specified, default is 'normal'
	if (maintid['num-rarity'] !== undefined && maintid['num-rarity'] !== '') {
		maintid.tags += (' rarity_' + maintid['num-rarity']);
	}
	else {
		maintid['num-rarity'] = 'normal';
		maintid.tags += ' rarity_normal';
	}
	
	if (maintid['num-sector'] !== undefined) {
		//onsole.log('NUM SECTOR', maintid['num-sector'], ' sector_' + strainIndex.codes[ maintid['num-sector'] ], ' sector_' + maintid['num-sector'] );
		
		//maintid.tags += (' sector_' + strainIndex.codes[ maintid['num-sector'] ] );
		maintid.tags += (' sector_' + maintid['num-sector'] );
		
		//onsole.log('STRAIN DEBUG', strainName, maintid['num-sector'], maintid['num-index']);
		
		// cache number for plugin
		// LATER: maybe remove in packed datafile
		if (strainIndex.codesReverse !== undefined) {
			maintid.number =
			heart.zerofill(strainIndex.codesReverse[ maintid['num-sector'] ], 2) + '-' + heart.zerofill(maintid['num-index'], 4);
		}
	}
	
	return maintid;
}; // }}}


const sortingFields = function(formtid, formStrain, strainIndex) { // {{{
	var strainName = formStrain['num-ident'];
	
	// LATER: don't store this in smaller version of data
	formtid['num-strain'] = strainName;
	
	/*
	 forms don't have numbers
	 but I want them to appear sorted as if they do
	 so.... this is in sorting-nnn.
	 which must be named sorting to avoid confusion with alignment genres aka sorts!
	*/
	
	formtid['sorting-sct'] = '' + strainIndex.codesReverse[ strainIndex.strains[ strainName ].q ];
	formtid['sorting-idx'] = '' + strainIndex.strains[ strainName ].i;
	formtid['sorting-rty'] = '' + strainIndex.rarity[ formStrain['num-rarity'] ].number;
	formtid['sorting-stg'] = '' + heart.stageSorting(formtid.stage);
	
	if (formtid['sorting-sct'] === undefined) {
		formtid['sorting-sct'] = '' + formStrain['num-sector'];
	}
	
	// zerofill sorting fields
	formtid['sorting-sct'] = heart.zerofill(formtid['sorting-sct'], 2);
	formtid['sorting-rty'] = heart.zerofill(formtid['sorting-rty'], 2);
	formtid['sorting-idx'] = heart.zerofill(formtid['sorting-idx'], 4);
}; // }}}


// generic module unpacker
exports.unpackModule = function(plugin) { // {{{
	var module, result;
	
	try {
		module = escapeModuleFormat(plugin.text);
		module = heart.hjsonParse(module);
		
		// unabbreviate '_DIR' structures
		module = exports.unAbbr(module);
		
		// parse 'enlist arrays'
		module = enlistFilter(module);
		
		// special cases
		module = moduleSpecialCases(module);
		
		// plugin readme displayed in tiddlywiki
		if (module.readme !== undefined) {
			module[plugin.title + '/readme'] =
			expandTidString(module.readme);
			delete module.readme;
		}
	}
	catch (e) {
		console.error('Trouble parsing strain!!', e, plugin);
		return;
	}
	
	result = plugin;
	result.tiddlers = module;
	
	return result;
}; // }}}

// module unpack helpers {{{
const escapeModuleFormat = function(plugin) {
	var result = plugin;
	
	result = result.replace(/(\[enlist\[[^\[\]\n]+\]\])/g, '\"$1\"');
	
	return result;
};

// implement space separated arrays
// this uses a syntax inspired by tiddlywiki filters, [enlist[ ... ]]
const enlistFilter = function(plugin) {
	var i, leaf, regex = /\[enlist\[(.+)\]\]/;
	
	for (i in plugin) {
		leaf = plugin[i];
		
		if (typeof leaf == 'string' && regex.test(leaf)) {
			leaf = leaf.replace(regex, '$1');
			leaf = leaf.replace(/\s+/g, ' ');
			leaf = leaf.split(' ');
		}
		else if (typeof leaf == 'object') {
			leaf = enlistFilter(leaf);
		}
		
		plugin[i] = leaf;
	}
	
	return plugin;
};

// special types of tids in modules
// 'pattern' tids, etc
const moduleSpecialCases = function(module) {
	var i, tid;
	
	for (i in module) {
		tid = module[i];
		
		if (typeof tid.pattern === 'object' && typeof tid.id === 'object') {
			// bake pattern name into object
			tid.name = i.replace(/.+\/([^\/]+)$/, '$1');
			
			tid = {
				text: JSON.stringify(tid),
				type: "application/json",
				"asekai-datatype": "kai-pattern"
			};
			
			module[i] = tid;
		}
	}
	
	return module;
};
// }}}

exports.unpackKaiStrain = function(strain, langNames, strainIndices) { // {{{
	var plugin, strainName, formStrain, strainIndex,
      langTids = [], languageList = {},
      i, k, la;
    const wiki = $tw.wiki;
    
    // func to tag a data sub-tid
    const addDataTag = function(tagname, dataTag) {
		if (plugin[tagname] === undefined) {
			plugin[tagname] = {
				"tags": 'k_' + strainName + ' k_' + strainName + '/' + dataTag + ' remove_in_min'
			}
		}
	};

	// parse different plugin formats {{{
	if (typeof strain == 'object' && strain.text !== undefined) {
		plugin = strain.text;
	}
	else if (strain instanceof Node) {
		plugin = heart.findPre(strain);
	}
	else {
		console.error('problem with strain ', strain);
		return;                                       
	}
	
	try {
		plugin = heart.hjsonParse(plugin);
	}
	catch (e) {
		console.error('Trouble parsing strain!!', e, plugin);
		return;
	}
	// }}}
	
	// set up language list
	for (i = 0; i < langNames.length; i++) {
		languageList[langNames[i]] = '';
	}
	
	
	// name of strain which will be used in tids
	strainName = strain.title.replace('$:/asekai/modules/strains/', '');
	
	// strain index
	strainIndex = strainIndices[ strain['strain-index'] ];
	
	// process strain main tid first
	// store ident in here so it doesn't have to be passed twice
	formStrain = plugin['k_' + strainName];
	formStrain['num-ident'] = strainName;
	plugin['k_' + strainName] =
		formStrain = strainMainTid(formStrain, strainIndex);
	
	
	// unabbr abbreviated strain // {{{
	if (plugin['k/' + strainName] !== undefined) {
		// k/strain is used for abbrs
		// because k_strain is already used
		// so temporarily switch things for unAbbr
		
		plugin['k_' + strainName + '/BACKUP'] = plugin['k_' + strainName];
		plugin['k_' + strainName] = plugin['k/' + strainName];
		delete plugin['k/' + strainName];
		
		plugin = exports.unAbbr(plugin);
		
		plugin['k_' + strainName] = plugin['k_' + strainName + '/BACKUP'];
		delete plugin['k_' + strainName + '/BACKUP'];
	} // }}}
	
	// if base form doesn't have a tid, create one
	if (plugin['k_' + strainName + '_' + strainName] === undefined) {
		plugin['k_' + strainName + '_' + strainName] = {
			align: "?/?/?",
			"num-sector": "?",
			stage: "?"
		};
	}
	
	
	// correct each tiddler
	for (i in plugin) {
		
		// kai form tids
		if (/^k_[a-z0-9]+_[a-z0-9]+$/.test(i)) {
			plugin[i]['asekai-datatype'] = 'k-form';
			plugin[i].tags = 'k_' + strainName;
			
			// process stage objects
			plugin[i] = stageObject(plugin[i]);
			
			// split align strings
			if (/\//.test(plugin[i].align) === true) {
				plugin[i].align = plugin[i].align.split('/');
				plugin[i]['align-shift'] = plugin[i].align[0];
				plugin[i]['align-phase'] = plugin[i].align[1];
				plugin[i]['align-sort'] = plugin[i].align[2];
				delete plugin[i].align;
			}
			
			
			// tag with sort
			if (plugin[i]['align-sort'] !== undefined) {
				if (plugin[i]['align-sort'] === '' || plugin[i]['align-sort'] === '?') {
					plugin[i]['align-sort'] = 'unknown';
				}
				
				plugin[i].tags += (' sort_' + plugin[i]['align-sort']);
			}
			
			var j;
			
			// if stage is youth make variant youth
			// this is because the pronunciation code looks at 'variant'
			if (plugin[i].stage == 'youth' && fieldIsEmpty(plugin[i].variant)) {
				plugin[i].variant = [ 'youth' ];
			}
			
			// tag with variant
			if (!fieldIsEmpty(plugin[i].variant)) {
				plugin[i]['variant'] = arrayifyString(plugin[i]['variant']);
				
				for (j = 0; j < plugin[i].variant.length; j++) {
					plugin[i].tags += (' variant_' + plugin[i]['variant'][j]);
				}
			}
			// if variant is empty make it array
			else {
				plugin[i]['variant'] = [];
			}
			
			// tag with strain rarity
			if (formStrain['num-rarity'] !== undefined) {
				plugin[i].tags += (' rarity_' + formStrain['num-rarity']);
			}
			
			// deal with sectors
			/* a kai form can have MULTIPLE sectors, in which case:
			   all the sectors should be tagged, then
			   the array should be stored in env-sectors
			   and the first sector, in num-sector
			*/
			if (fieldIsEmpty(plugin[i]['num-sector'])) {
				plugin[i]['num-sector'] = '?';
			}
			
			plugin[i]['env-sectors'] = arrayifyString(plugin[i]['num-sector']);
			
			// decode sector codes
			for (k = 0; k < plugin[i]['env-sectors'].length; k++) {
				if (strainIndex.codes[ plugin[i]['env-sectors'][k] ] !== undefined) {
					// decode code
					plugin[i]['env-sectors'][k] = strainIndex.codes[ plugin[i]['env-sectors'][k] ];
					// add tag
					plugin[i].tags += (' sector_' + plugin[i]['env-sectors'][k] );
				}
			}
			
			// collapse num-sector
			plugin[i]['num-sector'] = plugin[i]['env-sectors'][0];
			plugin[i]['env-sectors'] = plugin[i]['env-sectors'].join(' ');
			
			//onsole.log('NUM SECTOR', plugin[i]['num-sector'], plugin[i]['env-sectors'], plugin[i].tags);
			
			
			//onsole.log('SORTING DEBUG', plugin[i]['sorting-sct'] + '-' + plugin[i]['sorting-rty'] + '-' + plugin[i]['sorting-idx'] + '-' + plugin[i]['sorting-stg']);
			//onsole.log('INDEX', strainIndex);
			//
			
		}
		// usually these will be specified in an abbreviated 'l10n' obj....
		// but if not, catch them
		else if (
			/\$\:\/asekai\/lang\/[a-z]+\/(strains|describe-strain)/.test(i)
		) {
			langTids.push(i);
		}
		// sprites
		else if (/Sprites\//.test(i)) {
			plugin[i] = expandTidString(plugin[i]);
			
			plugin[i].tags = 'Sprites/' + strainName;
			plugin[i].type = 'image/svg+xml';
		}
		// palettes
		else if (/p_.+/.test(i)) {
			if (/p_basic/.test(i)) {
				plugin[i].tags = 'p_basic k_' + strainName + '/palettes';
			}
			else {
				plugin[i].tags = 'Palettes k_' + strainName + '/palettes';
			}
		}
		
		
		// either strain or form tid
		if (/^k_[a-z0-9]+(_[a-z0-9]+)?$/.test(i)) {
			
			// create art tag
			// Art tags should include k_ to distinguish them
			// from Art tags for general things that aren't kai
			plugin['Art/' + i] = {
				title: 'Art/' + i,
				tags: 'Art only_in_guide'
			};
			
			// sorting fields
			sortingFields(plugin[i], formStrain, strainIndex);
			
			if (plugin[i]['asekai-datatype'] == 'k-strain') {
				plugin[i]['num-basic'] = 'k_' + strainName + '_' + strainName;
			}
			
			// unpack l10n objects
			if (plugin[i].l10n !== undefined || plugin[i].inherit !== undefined) {
				// the way inherit works there must be a lang tid marked inherit
				// but that won't be defined in the lang plugin
				// so create that here
				if (plugin[i].inherit !== undefined) {
					//onsole.log('lang test', plugin[i]);
					
					if (plugin[i].l10n === undefined) {
						plugin[i].l10n = {};
					}
					
					for (la in languageList) {
						if (plugin[i].l10n[la] === undefined) {
							plugin[i].l10n[la] = {};
						}
						
						if (plugin[i].l10n[la] !== undefined) {
							plugin[i].l10n[la].inherit = plugin[i].inherit;
						}
					}
				}
				
				
				for (la in plugin[i].l10n) {
					// pick up descriptions
					if (plugin[i].l10n[la].summary !== undefined) {
						for (k in plugin[i].l10n[la].summary) {
							//onsole.log('SUMMARY DEBUG', '$:/asekai/lang/' + la + '/form-summary/' + i.replace('k_', '') + '/' + k, expandTidString( plugin[i].l10n[la].summary[k]));
							
							plugin['$:/asekai/lang/' + la + '/form-summary/' + i.replace('k_', '') + '/' + k] =
							expandTidString( plugin[i].l10n[la].summary[k]);
							langTids.push('$:/asekai/lang/' + la + '/form-summary/' + i.replace('k_', '') + '/' + k);
						}
					}
					
					delete plugin[i].l10n[la].summary;
					
					// store most l10n strings in one tid
					plugin['$:/asekai/lang/' + la + '/strains/' + i.replace('k_', '')] = plugin[i].l10n[la];
					langTids.push('$:/asekai/lang/' + la + '/strains/' + i.replace('k_', ''));
				}
				
				delete plugin[i].l10n;
			}
		}
		
		
		// tag data tids for hiding
		// FIXME: these arent hidden properly in guidefile.
		if (/k_[a-z0-9]+(_[a-z0-9]+)?\/[a-z]+(\/[a-z]+)?/.test(i)) {
			if (/k_[a-z0-9]+/.test(plugin[i].tags) === false) {
				plugin[i].tags += (' k_' + strainName);
			}
			
			plugin[i].tags += (' k_' + strainName + '/data');
		}
	}
	
	
	// process language tids
	for (k = 0; k < langTids.length; k++) {
		i = langTids[k];
		
		if (plugin[i] === undefined) {
			console.error('lang tid', k, 'is undefined');
			continue;
		}
		
		plugin[i] = expandTidString(plugin[i]);
		
		plugin[i].title = i;                
		plugin[i].tags = 'k_' + strainName + '/strings';
		
		if (/\$\:\/asekai\/lang\/[a-z]+\/strains/.test(i)) {
			plugin[i] = l10nTid(plugin[i]);
		}
	}
	
	
	// readme
	if (plugin.readme !== undefined) {
		plugin['$:/asekai/modules/strains/' + strainName + '/readme'] =
		expandTidString(plugin.readme);
		
		delete plugin.readme;
	}
	
	// meta chart
	if (plugin.meta !== undefined) {
		if (typeof plugin.meta == 'object') {
			plugin.meta = JSON.stringify(plugin.meta);
		}
		
		plugin['k_' + strainName + '/meta'] = {
			tags: 'k_' + strainName + ' Metas' + ' k_' + strainName + '/data',
			type: 'application/json',
			text: plugin.meta
		};
		
		//onsole.log('META DEBUG', plugin.meta);
		delete plugin.meta;
	}
	
	// summary (sector descriptions)
	if (plugin.summary !== undefined) {
		for (la in plugin.summary) {
			for (k in plugin.summary[la]) {
				plugin['$:/asekai/lang/' + la + '/strain-summary/' + strainName + '/' + k] =
				expandTidString(plugin.summary[la][k]);
			}
		}
		
		delete plugin.summary;
	}
	
	// sigils
	if (plugin.sigils !== undefined && typeof plugin.sigils == 'string') {
		plugin['k_' + strainName + '/sigils'] = {
			tags: 'k_' + strainName + ' k_' + strainName + '/data',
			text: plugin.sigils
		};
		
		delete plugin.sigils;
	}
	
	// kai icons
	if (plugin['k_' + strainName + '/icons'] === undefined)
	{	
		plugin['k_' + strainName + '/icons'] = {
			tags: 'k_' + strainName + '/sprites remove_in_min',
			type: 'application/json'
		};
		
		if (plugin.icons === undefined) {
			plugin.icons = {};
			plugin.icons[strainName] = 'side';
		}
		
		// add icon field to each tid
		for (k in plugin.icons) {
			plugin['k_' + strainName + '_' + k].icon = plugin.icons[k];
		}
		
		plugin['k_' + strainName + '/icons'].text = plugin.icons;
		delete plugin.icons;
	}
	
	// icon for whole plugin
	if (plugin[ 'Sprites/' + strainName + '/' + plugin['k_' + strainName + '/icons'].text[strainName] ] !== undefined)
	{
		plugin['$:/asekai/modules/strains/' + strainName + '/icon'] = {
			text: '{{Sprites/' + strainName + '/' + plugin['k_' + strainName + '/icons'].text[strainName] + '}}'
		};
		// type: 'image/svg+xml',
		// text: plugin[ 'Sprites/' + strainName + '/' + plugin['k_' + strainName + '/icons'].text[strainName] ].text
	}
	
	plugin['k_' + strainName + '/icons'].text = JSON.stringify( plugin['k_' + strainName + '/icons'].text );
	
	
	// add org tags that will be hidden in guidefile
	addDataTag('k_' + strainName + '/strings', 'data');
	addDataTag('k_' + strainName + '/sprites', 'data');
	addDataTag('k_' + strainName + '/palettes', 'data');
	addDataTag('Sprites/' + strainName, 'sprites Sprites');
	//
		

	//onsole.log('KAI STRAIN forEachModuleOfType', strains[j]);
	//onsole.log('KAI STRAIN', JSON.stringify(plugin, null, 2));
	
	strain.tiddlers = plugin;
	
	return strain;
}; // }}}

// expandtidstring, addshadows {{{

var fieldIsEmpty = function(field) {
	if (field === undefined || field == '' ||
	(typeof field == 'object' && field.constructor.name == 'Array' && field.length < 1)
	) {
		return true;
	}
	
	return false;
};

const expandTidString = function(str) {
	if (typeof str == 'string') {
		return {
			text: str
		};
	}
	
	return str;
},

arrayifyString = function(string) {
	var result;
	
	if (typeof string == 'object' && string.constructor.name == 'Array') {
		result = string;
	}
	else {
		result = [ string ];
	}
	
	return result;
},

addShadows = function(plugin) {
	var tiddlers = plugin.tiddlers, k;
	
	// create shadow tiddlers
	for (k in tiddlers) {
		tiddlers[k].title = k;
		tiddlers[k].created = plugin.created;
		tiddlers[k].modified = plugin.modified;
		
		$tw.wiki.addShadowTiddler(tiddlers[k], plugin.title);
	}
};

// }}}

exports.unpackLanguage = function(language) { // {{{
	var plugin, la, ident,
	    inheritTitle, inheritTitleShort, longTitle,
	    prefix, prefix2,
	    i, j, k;
	
	const kaiRegex = /^((?:strains)\/)(.+)$/;
	
	try {
		la = language.title.replace(/^\$:\/asekai\/language\/([a-zA-Z0-9-]+)(?:\/.+)?/, '$1');
		
		plugin = Hjson.parse(language.text);
		//onsole.log('pre-unpack language', plugin);
		
		// unpack 'dir', 'strain' structure: 
		for (i in plugin) {
			// for each '_DIR'
			if (typeof plugin[i] == 'object' && plugin[i]._DIR == 'dir') {
				prefix = '$:/asekai/lang/'+ la +'/' + i;
				delete plugin[i]._DIR;
				
				// for each 'strain'
				for (j in plugin[i]) {
					if (typeof plugin[i][j] == 'object' && plugin[i][j]._DIR == 'strain') {	
						delete plugin[i][j]._DIR;
						
						// for each form
						for (k in plugin[i][j]) {
							prefix2 = prefix + '/' + j + '_';
								
							plugin[prefix2 + k] = plugin[i][j][k];
						}
					}
				}
				
				delete plugin[i];
			}
		}
		//
		
		
		for (i in plugin) {
			plugin[i] = expandTidString(plugin[i]);
			
			// strains, describe-strain
			if (kaiRegex.test(i)) {
				ident = i.replace(kaiRegex, '$2');
				
				// put tid under its respective kai
				plugin[i].tags = 'k_' + util.identStrain(ident) + '/strings';
				
				longTitle = '$:/asekai/lang/'+ la +'/' + i;
			}
			else if (/(sectors|sorts|variants)\//.test(i)) {
				// tags?
				
				longTitle = '$:/asekai/lang/'+ la +'/' + i;
			}
			else {
				longTitle = i;
			}
			
			plugin[i].title = longTitle;
			plugin[longTitle] = plugin[i];
			
			if (i !== longTitle) {
				delete plugin[i];
			}
			
			// process special things
			plugin[longTitle] = l10nTid(plugin[longTitle]);
		}
		
		// readme
		if (plugin.readme !== undefined) {
			plugin[language.title + '/readme'] = expandTidString(plugin.readme);
			delete plugin.readme;
		}
		
		if (plugin.language !== undefined && plugin.language.text === undefined) {
			plugin['$:/asekai/lang/'+ la +'/language'] = {
				text: JSON.stringify(plugin.language)
			};
			
			delete plugin.language;
		}
		
		//onsole.log('unpacked language', plugin);
	}
	catch (e) {
		console.error('error unpacking language', e);
		console.error(language.text);
	}
	
	
	language.tiddlers = plugin;
	
	return language;

}; // }}}

var stageObject = function(tid) { // {{{
	const unabbrevs = {
		n: 'stage-label',
		m: 'stage-bad',
		b: 'stage'
	};
	var stage = tid.stage, i;
	
	if (fieldIsEmpty(stage)) {
		return tid;
	}
	else if (typeof stage == 'string') {
		stage = { b: stage };
	}
	
	// mark empty string as boxless
	// so "stage" will always get flattened to a string.
	if (fieldIsEmpty(stage.b)) {
		stage.b = '+';
	}
	
	// unabbreviate each
	for (i in unabbrevs) {
		if ( !fieldIsEmpty(stage[i]) ) {
			tid[ unabbrevs[i] ] = stage[i];
		}
	}
	
	//if (tid.tags == 'k_valenoern') { onsole.log('STAGEOBJECT', tid); }
	return tid;
}; // }}}

// process special shorthands for l10n tids
var l10nTid = function(langTid) { // {{{
	if (/\$\:\/asekai\/lang\/[a-z]+\/strains/.test(langTid.title)) {
		if (typeof langTid.name == 'object') {
			langTid['name-variant'] = langTid.name.v;
			langTid['name-plural'] = langTid.name.pl;
			
			// allow 'p' for either post-'extended' or postshort if the other is defined
			if (langTid.name.p !== undefined) {
				if (langTid.name.ps !== undefined && langTid.name.pe === undefined) {
					langTid.name.pe = langTid.name.p;
				}
				else if (langTid.name.pe !== undefined && langTid.name.ps === undefined) {
					langTid.name.ps = langTid.name.p;
				}
			}
			
			langTid['name-post'] = langTid.name.pe;
			langTid['name-postshort'] = langTid.name.ps;
			
			var unabbrevs = {
				name: {
					pron: 'name-pron',
					mean: 'name-mean',
					etym: 'name-etym'
				}
			}, i;
			
			// unabbreviate 'name' structure
			//  ex. if langtid.name.pron defined, langtid.name-pron = "
			for (i in unabbrevs.name)  {
				if (!fieldIsEmpty(langTid.name[i])) {
					langTid[ unabbrevs.name[i] ] = langTid.name[i];
				}
			}
			
			// collapse name
			langTid.name = langTid.name.n;
		}
		// otherwise name is already defined
		
		if (typeof langTid['name-pron'] == 'object') {
			langTid['name-pron'] = JSON.stringify(langTid['name-pron']);
		}
		
		// deal with shorthands
		if (langTid['name-plural'] === '"') {
			langTid['name-plural'] = langTid['name'];
		}
		
	}
	
	return langTid;
}; // }}}

exports.parseStrainIndex = function(indexTid) { // {{{
	var mimetype, body,
	    index = {}, indexNo, strainName,
	    i, j;
	
	if (typeof indexTid == 'string') {
		body = indexTid;
	}
	else {
		body = indexTid.fields.text;
		mimetype = indexTid.fields['type'];
	}
	
	body = Hjson.parse(body);
	index.codes = {}; // sectors by codes
	index.codesReverse = {}; // codes by sector
	index.strains = {}; // numbers for strains
	index.sectors = {};
	
	
	// if rarity object is defined, use it {{{
	// otherwise make default rarity object
	if (body._rarity !== undefined) {
		index.rarity = body._rarity;
	}
	else {
		index.rarity = {
			order: [ 'normal', 'legend', 'elemental' ]
		}
	}
	
	for (i = 0; i < index.rarity.order.length; i++) {
		if (index.rarity[ index.rarity.order[i] ] === undefined) {
			index.rarity[ index.rarity.order[i] ] = {
			};
		}
		
		index.rarity[ index.rarity.order[i] ].number = i;
	}
	// }}}
	
	
	for (i in body) {
		// tracks numbers, is reset every sector
		// needs to be separate from j due to ability to manually set kai number
		indexNo = 1;
		
		// if key contains an underscore, it is not a sector
		if (/^_/.test(i)) {
			continue;
		}
		
		// "order" lists the strains by number
		if (body[i].order) {
			for (j = 0; j < body[i].order.length; j++) {
				strainName = body[i].order[j];
				
				/*if (body[i].spec[ body[i].order[j] ] !== undefined) {
					indexNo = body[i].spec[ body[i].order[j] ];
				}*/
				// for indexes with gaps (when in development etc),
				// a specific number may be specified like [ 'tripod/3' ]
				if (/\/[0-9]+$/.test(strainName) === true) {
					indexNo = strainName.replace(/^(.+)\/([0-9]+)$/, '$2') * 1;
					strainName = strainName.replace(/^(.+)\/([0-9]+)$/, '$1');
				}
				
				// set 'master number' + individual sector numbers
				// if list of all valid numbers is empty, create it
				if (index.strains[strainName] === undefined) {
					index.strains[strainName] = {
						'q': i, 'i': indexNo,
						'sectors': {}
					};
				}
				
				index.strains[strainName].sectors[i] = indexNo;
				//
				
				indexNo++;
			}
		}
		
		
		if (body[i].order === undefined) {
			body[i].order = [];
		}
		
		if (body[i].list === undefined && body[i].order !== undefined) {
			body[i].list = body[i].order;
		}
		else if (body[i].list === undefined) {
			body[i].list = [];
		}
		
		index.sectors[i] = {
			list: body[i].list,
			order: body[i].order
		};
		
		// make sector code lookup-able
		if (body[i].code !== undefined) {
			index.codes['' + body[i].code] = i;
			index.codesReverse[i] = '' + body[i].code;
		}
	}
	
	// fallback codes
	index.codes['?'] = 'unknown';
	
	//onsole.log('INDEX', index);
	return index;
} // }}}

// unloads a plugin's tiddlers into a 'bag' object only if not already in it;
// modifies 'bag'.
const intoTidbagOnce = function(plugin, tidBag) {
	var i;
	
	for (i in plugin.tiddlers) {
		if (tidBag[i] === undefined) {
			tidBag[i] = plugin.tiddlers[i];
			tidBag[i].plugin = plugin;
		}
	}
};

// func that runs at tw startup to unpack kai strains
exports.startup = function() { // {{{
	var strains, languages, langNames = [], strainIndices = {},
		plugins, tidBag = {},
	    i, j;
	
	const wiki = $tw.wiki;
	
	try {
		// unpack generic modules
		var genericModules = getKaiPlugins('asekai-module');
		
		for (j = 0; j < genericModules.length; j++) {
			genericModules[j] = exports.unpackModule(genericModules[j]);
			intoTidbagOnce(genericModules[j], tidBag);
		}
		
		//onsole.log('TIDBAG', tidBag);
		
		
		// unpack language plugins
		languages = getKaiPlugins('kai-lang');
		
		for (j = 0; j < languages.length; j++) {
			if (/\/basic$/.test(languages[j].title) === true) {
				langNames.push(languages[j].title.replace(/.+\/([A-Za-z-]+)\/basic$/, '$1'));
			}
			
			languages[j] = exports.unpackLanguage(languages[j]);
			intoTidbagOnce(languages[j], tidBag);
		}
		//
		
		// unpack each strain
		// the unpacking func can easily be required & used elsewhere
		strains = getKaiPlugins('kai-strain');
		
		for (j = 0; j < strains.length; j++) {
			// unpack strain index specified by plugin
			if (strainIndices[ strains[j]['strain-index'] ] === undefined) {
				strainIndices[ strains[j]['strain-index'] ] =
				exports.parseStrainIndex( wiki.getTiddler( strains[j]['strain-index'] ) );
			}
			
			strains[j] = exports.unpackKaiStrain(strains[j], langNames, strainIndices);
			
			for (i in strains[j].tiddlers) {
				if (tidBag[i] === undefined) {
					tidBag[i] = strains[j].tiddlers[i];
					tidBag[i].plugin = strains[j];
				}
				else if (strains[j].tiddlers[i].inherit !== undefined) {
					tidBag[i].inherit = strains[j].tiddlers[i].inherit;
					delete strains[j].tiddlers[i];
				}
			}
		}
		//
		
		
		// implement inherit
		var inheritTid, inheritTitle, inheritTo, source, formStrain;
		
		// implement tids that inherit from another kai
		for (i in tidBag) {
			if (tidBag[i].inherit !== undefined) {
				if (tidBag[i].title === undefined) {
					tidBag[i].title = i;
				}
				
				inheritTo = tidBag[i];
				inheritTitle = inheritTo.title.replace(/^(\$:\/asekai\/.+\/|k_)(.+)/, '$1' + inheritTo.inherit);
				inheritTid = tidBag[inheritTitle];
				
				if (inheritTid !== undefined) {
					for (j in inheritTid) {
						if (inheritTo[j] === undefined && inheritTid[j] !== undefined && /created|modified/.test(j) === false) {
							inheritTo[j] = inheritTid[j];
						}
					}
					
					if (/\$:\/asekai\/lang\//.test(inheritTo.title)) {
						delete inheritTo.inherit;
					}
				}
			}

			tidBag[i].plugin[i] = inheritTo;
			
			// fill empty phenoclass
			if (/\$\:\/asekai\/lang\/[a-z]+\/strains/.test(i)) {
				formStrain = tidBag[i.replace(/_[a-z0-9]+/, '')];
				
				if (fieldIsEmpty(tidBag[i].phenoclass)) {
					//onsole.log('tidbag', tidBag[i], formStrain);
					tidBag[i].phenoclass = formStrain.phenoclass;
				}
			}
			
			delete tidBag[i].plugin;
		}
		
		//onsole.log('tidbag', tidBag);
		
		// join languages and plugins and then make their shadows
		plugins = genericModules.concat(languages, strains);
		
		for (j = 0; j < plugins.length; j++) {
			addShadows(plugins[j]);
		}
		
		// the edited boot script makes kai strains work like regular plugins
		// by making shadows and attaching them to the strain plugins
		// so, all that has to be done is "unpack plugin tiddlers" like normal
		wiki.readPluginInfo();
		wiki.registerPluginTiddlers('asekai-module');
		wiki.registerPluginTiddlers('kai-lang');
		wiki.registerPluginTiddlers('kai-strain');
		wiki.unpackPluginTiddlers();
		
	}
	catch (e) {
		console.error('kai-strain.js error', e);
	}
	
}; // }}}

})();
