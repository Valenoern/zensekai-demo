/*\
jedit: :folding=explicit: {{{
created: 20191223034549191
title: $:/asekai/datafile/macros/boxchart.js
tags: 
type: application/javascript
module-type: macro

draws kai form chart based on pattern data

\*/
(function(){
/*jslint node: true, browser: true, esnext: true */
/*global $tw: false */
"use strict";

/* Information about this macro */
exports.name = "boxchart";
exports.params = [ {name: "ident"} ];

var kaiLangString, PluginDecoder; // {{{
//const nodeFound = (process.release.name === 'node' || !$tw);
const nodeFound = true;

if (nodeFound) {
	kaiLangString = require('$:/asekai/datafile/macros/kai-lang-string').run;
	PluginDecoder = require('$:/asekai/datafile/lib/plugin-decoder.js').PluginDecoder;
}
// if nodejs isn't found, run a simple test function instead of requiring
else {
	kaiLangString = function(ident, field) {
		var test = 
		'Balto (legendary sled dog) -> Alto, circus ("circle", la) + alto ("high", it)'
		; return test;
	};
}
// }}}

// de-structure library back to 'regular' functions
var { fieldIsEmpty } = PluginDecoder;

// }}}


/* Box, Drawer {{{ */
class Box { // {{{
	constructor(form) {
		var stage = form.stage;
		
		if (form.title.length > 1 && form.placeholder !== true) {
			this.caption = this.kaiName(form.title);
		}
		else {
			this.caption = "?";
		}
		
		this.stage = stage;
		this.label = form['stage-label']
		this.id = 'b_' + stage;
		this.placeholder = form.placeholder;
		
		if (typeof stage !== 'string') {
			console.log('BOX', this.stage);
		}
	}
	
	kaiName(tidTitle) {
		var name, caption;
		
		name = kaiLangString(tidTitle, 'name-chart');
		// TODO: add variant icon
		
		caption = name;
		
		return caption;
	}
	
	toString() {
		var emptyClass = '', label = this.stage,
		    frame = '<use href="#boxframe-frame" class="b-frame" />';
		
		if (this.placeholder === true) {
			emptyClass = ' box-placeholder';
			// TODO: add some kinda question mark graphic later
			//frame = '<rect class="bkg" x="0" y="0" /><rect class="sprite" x="0" y="0" />';
		}
		
		if (this.label !== undefined) {
			label = this.label;
		}
		
		frame += ('<g class="label"><use href="#boxframe-label" class="b-label" /><text>' + label + '</text></g>');
		
		return '' +
		'\t<g id="chart-' + this.id + '" role="node" class="box ' + this.id + emptyClass + '">\n' +
		'\t  ' + frame + '  <text class="caption">' + this.caption + '</text>\n' +
		'\t</g>';
	}
} // }}}

class Drawer { // {{{
	constructor(name) {
		this.name = name;
		this.boxes = [];
	}
	
	push(box) {
		this.boxes.push(box);
	}
	
	toString() {
		var guts = [], i;
		
		for (i = 0; i < this.boxes.length; i++) {
			//onsole.log('DRAWER BOXES', this.boxes[i]); 
			guts.push(this.boxes[i].toString());
		}
		
		if (guts.length < 1) {
			return '';
		}
		
		return '<g class="drawer d_' + this.name + '">\n' +
		guts.join('\n') +
		'\n</g>';
	}
} // }}}

class Chart { // {{{
	constructor(pattern) {
		var i;
		
		this.pattern = pattern;
		
		this.drawerNames = {};
		this.drawerList = [];
		
		this.filledBoxes = [];
		
		for (i in pattern.drawers) {
			this.addDrawer(new Drawer(i));
		}
	}
	
	addDrawer(drawer) {
		this.drawerNames[ drawer.name ] = drawer;
		this.drawerList.push(drawer);
	}
	
	addBox(box) {
		var stage = box.stage, drawer;
		
		// put box into its respective drawer if it exists
		if (this.pattern.map[stage] !== undefined) {
			drawer = this.pattern.map[stage];
		}
		else {
			drawer = 'none';
		}
		
		if (this.pattern.id[ box.stage ] !== undefined) {
			box.id = 'b_' + this.pattern.id[ box.stage ];
		}
		
		this.drawerNames[ drawer ].push(box);
		this.filledBoxes.push(box.stage);
		// LATER: when there are two variations of a box row make one fill both
	}
	
	emptyBoxes() {
		var i, pattern = this.pattern, spareChart;
		const noneTest = false;
		
		spareChart = new Chart(pattern);
		//onsole.log('PATHEREHR', pattern);
		
		// when testing none boxes.
		if (noneTest === true) {
			for (i = 0; i < 12; i++) {
				spareChart.addBox(new Box({ "stage": "+", "title": "", "placeholder": true }));
			}
		}
		
		for (i in pattern.map) {
			// optional boxes in pattern.optional won't be added
			// nor will any box in pattern.drawers.none - which are considered 'boxless'
			if (pattern.drawers.none.indexOf(i) === -1 && pattern.optional.indexOf(i) === -1 && this.filledBoxes.indexOf(i) === -1) {
				spareChart.addBox(new Box({ "stage": i, "title": "", "placeholder": true }));
			}
		}
		
		return spareChart;
	}
	
	connectors() {
		// connectors don't actually work in browsers oops
		/*return '' +
		'<connector role="edge" n1="#chart-m1" n2="#chart-m2" />\n' +
		'<connector role="edge" n1="#chart-m2" n2="#chart-m3" />\n' +
		'<connector role="edge" n1="#chart-m3" n2="#chart-m4" />';*/
		return '';
	}
	
	toString() {
		var spareChart = this.emptyBoxes(), guts = [], drawer, i;
		
		for (i in this.drawerNames) {
			drawer = spareChart.drawerNames[i].toString() + this.drawerNames[i].toString();
			
			guts.push(drawer);
		}
		
		return '<svg class="form-chart" viewBox="0 0 150 95">\n' +
		'<defs>\n' +
		'<g id="boxframe-frame" class="box"><rect class="bkg" x="0" y="0" width="8em" height="7.5em" /><rect class="sprite" x="0" y="0" width="7em" height="7em" /></g>\n' +
		'<g id="boxframe-label" class="box"><path class="tab" d="m -10.3,-4.5 3.4,-3.6 h4.0 v3.6 z" /><path class="erase" d="m-11,-4.3 v-.5 h7.7 v-4 h.6 v4.5 z" /></g>\n' +
		'<pattern id="grid" x="0" y="0"><rect x="0" y="0" /></pattern>\n' +
		'</defs>\n' + 
		'<g class="chart-body pattern_' + this.pattern.name + '">\n' +
		'<rect class="frame" x="0" y="0" width="149" height="94" />\n' +
		guts.join('\n') + '\n' +
		'</g>\n' +
		this.connectors() +
		'</svg>';
	}
} // }}}
// }}}


// transform pattern map into simple lookup table
var processPatternMap = function(map) { // {{{
	var pattern, result = {}, i, j;
	
	pattern = map.pattern;
	
	for (i in pattern) {
		//onsole.log('MAP I', pattern[i]);
		
		for (j = 0; j < pattern[i].length; j++) {
			result[ pattern[i][j] ] = i;
		}
	}
	
	// rename properties mainly to avoid 'pattern.pattern'
	map.drawers = map.pattern;
	delete map.pattern;
	map.map = result;
	
	return map;
}; // }}}

// direct 
// TODO: bypass filterTiddlers somehow
exports.chart = function(ident) {
	return exports.run(ident);
};

/* Run the macro - <<boxchart "ident">> */
exports.run = function(ident) {
	var result, formTids, i,  drawers, chartGuts;
	const wiki = $tw.wiki;
	
	formTids = wiki.filterTiddlers('[title[' + ident + ']tagging[]field:asekai-datatype[k-form]]');
	drawers = {};
	
	// retrieve pattern name
	var kaiTid, patternName = 'variant';
	kaiTid = wiki.getTiddler(ident);
	
	if (kaiTid.fields.pattern !== undefined) {
		patternName = kaiTid.fields.pattern;
	}
	
	// retrieve pattern data
	var pattern, chart, patternList, patternTid;
	
	patternList = wiki.filterTiddlers('[suffix[' + patternName + ']] $:/asekai-modules/basic/patterns/' + patternName + ' +[limit[1]]');
	patternTid = wiki.getTiddler(patternList);
	pattern = JSON.parse(patternTid.fields.text);
	
	pattern = processPatternMap(pattern);
	chart = new Chart(pattern);

	var form, box;
	
	for (i = 0; i < formTids.length; i++) {
		form = wiki.getTiddler(formTids[i]);
		
		box = new Box(form.fields);
		chart.addBox(box);
	}
	
	//onsole.log('BOXCHART', chart);
	
	result = chart.toString();
	//onsole.log('RESULT', result);
	return result;
};

})();