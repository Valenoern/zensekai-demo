author: Takumi
core-version: >=5.0.0
created: 20190322093315868
description: Asekai strain "Remaliat"
list: readme
plugin-type: kai-strain
strain-index: $:/asekai/modules/strain-index
tags: kai-strain k_remaliat
title: $:/asekai/modules/strains/remaliat
type: application/hjson
version: 20.04.14

// :wrap=soft:
{

readme:
"asekai basic strains: remaliat"


k_remaliat: {
	number: { s: 10 }
	
	tags: [ 'a_pieces' ]
	
	l10n: { en: {
		phenoclass: "scrap sculpture"
	}}
}


sigils: "|!goodroute||\n|!badroute||"

icons: {
	remaliat: side
}

meta: {
	youth: [
		{ to: "remaliat",  type: "grow",  time: 1 }
		{ to: "peat",      type: "grow",  time: 1,  kerv: 10 }
	]
	
	remaliat: [
		{ to: "peat",     type: "curve",   hide: false }
	]
	
	peat: [
		{ to: "scaletree", type: "level",  levels: 10 }
	]
	
	scaletree: [
		{ to: "strata",    type: "level",  levels: 10 }
	]
	
	strata: [
		{ to: "coal",      type: "level",  levels: 10 }
	]
	
	coal: [
	]
}



summary: { en: {
	default:
	"Our REMALIAT is a cutting-edge technological kai made of 100% recycled materials. No 'food', no 'rest', no 'trait adjustment' required. Feed it your recyclables and watch it grow! Picking up cans and bottles on the side of the road has never sounded so appealing."
}}



"k/remaliat": { "_DIR": "strain"

youth: {
	align: "4/5/dpep"
	num-sector: 1
	stage: b0
	
	l10n: { en: {
		name: { v: "Process", n: "Remaliat"
			pron: [ "" ]
			mean: ""
			etym: ''
		}
	}}
}


remaliat: {
	align: "4/5/dpep"
	num-sector: [ 1, 3 ]
	stage: b
	
	l10n: { en: {
		name: { n: "Remaliat"
			pron: [ "ɹɛˈmɑli.ət" ]
			mean: "recycled material"
			etym: 'anagram of "material"'
		}
	}}
}



peat: {
	align: "4.5/4.5/maya"
	num-sector: 1
	stage: m1
	stage-kerv: true
	
	l10n: { en: {
		name: { n: "Possemat"
			pron: [ "pɒsɛmɒt" ]
			mean: "remaliat that is able / peat moss"
			etym: 'anagram of "peat moss"'
		}
	}}
}

scaletree: {
	align: "4.5/4.5/maya"
	num-sector: 1
	stage: m2
	stage-kerv: true
	
	l10n: { en: {
		name: { n: "Stelecrea"
			pron: [ "stɛlɛ.kɹiː.ə" ]
			mean: "creator of pillars/tombstones / scale tree"
			etym: 'anagram of "scale tree"'
		}
	}}
}


strata: {
	align: "4.5/4.5/sass"
	num-sector: "2a"
	stage: m3
	stage-kerv: true
	
	l10n: { en: {
		name: { n: "Yrratelaast"
			pron: [ "ɪɹə.tɛ.læst" ]
			mean: "era to last / strata layer"
			etym: 'anagram of "strata layer"'
		}
	}}
}


coal: {
	align: "4.5/4.5/synk"
	num-sector: "2a"
	stage: m4
	stage-kerv: true
	
	l10n: { en: {
		name: { n: "Ocamseal"
			pron: [ "ɒkəm.siːəl" ]
			mean: "seal of simplest solutions / coal seam"
			etym: 'anagram of "coal seam"'
		}
	}}
}

}



}