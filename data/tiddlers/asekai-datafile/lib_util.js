/*\
jedit: :folding=explicit:
created: 20190313111217631
tags: Plugins
title: $:/asekai/datafile/util.js
type: application/javascript
module-type: library

util.js

\*/
(function(){
/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

/* Information about this macro */
var util = {

name: "util"

, identFull: function(ident) {
	if (/([a-z0-9]{2,})_([a-z0-9]+)/.test(ident) === true) {
		return ident.replace(/^(?:k_)?([a-z0-9_]+)$/, '$1');
	}
	
	return ident.replace(/^(?:k_)?([a-z0-9]+)$/, '$1_$1');
}

, identStrain: function(ident) {
	return ident.replace(/^(?:k_)?([a-z0-9]+)(_[a-z0-9]+)?/, '$1');
}


, zerofill: function(no, places) {
	var i;
	no = no + '';
	
	if (no.length < places) {
		for (i = (places - no.length); i > 0; i--) {
			no = '0' + no;
		}
	}
	
	return no;
}


};

exports.util = util;

})();