/*\
jedit: :folding=explicit:
created: 20190310113551195
tags: Plugins
title: $:/asekai/datafile/metas-vis
type: application/javascript
module-type: macro

visjs meta chart

\*/
(function(){
/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var vis = require('$:/plugins/felixhayashi/vis/vis.js');
var util = require('$:/asekai/datafile/util.js').util;
var kaiLangString = require('$:/asekai/datafile/macros/kai-lang-string').run;


/* Information about this macro */
exports.name = "kai-metas-vis";
exports.params = [ {name: "ident"} ];


var visData = function(forms, metas, kaiInfo) { // {{{
	var nodes = [], edges = [], data,
	    node, edge,
	    i, j;
	    
	for (i = 0; i < forms.length; i++) {
		node = { id: i, label: forms[i] };

		if ( kaiInfo[ forms[i] ].name !== undefined && kaiInfo[ forms[i] ].name !== '' ) {
			node.label = kaiInfo[ forms[i] ].name;
		}
		
		// place 'youth' at the top
		if (forms[i] == 'youth') {
			node.y = 0;
			node.mass = 5;
		}
		
		if (kaiInfo[ forms[i] ] !== undefined && kaiInfo[ forms[i] ].kerv === true) {
			node.color = { background: '#FC9797' };
			
			if (kaiInfo[ forms[i] ].stage !== undefined && /m[1-4]/.test(kaiInfo[ forms[i] ].stage) === false) {
				node.mass = 4;
				// try to make kerv nodes repel more
			}
		}
		
		nodes.push(node);
	
		if (metas[forms[i]] !== undefined) {
			for (j = 0; j < metas[forms[i]].length; j++) {
				// some edges will clutter the chart, so hide them
				// they will still be shown in the table on kai form pages
				if (metas[forms[i]][j].hide === true) {
					continue;
				}
				
				edge = {
					from: i,
					to: forms.indexOf( metas[forms[i]][j].to ),
					arrows: 'to',
					label: metas[forms[i]][j].type
				};
				
				if (/^(level|curve)$/.test( metas[ forms[i] ][j].type )) {
					edge.color = { color: 'red' };
				}
				else if (/^(trinity)$/.test( metas[ forms[i] ][j].type )) {
					edge.color = { color: 'green' };
				}
				
				if (metas[forms[i]][j].twoarrow === true || /^(elemental)$/.test( metas[ forms[i] ][j].type )) {
					edge.arrows = 'to, from';
				}
				
				edges.push(edge);
			}
		}
	
	}
	
	//onsole.log('DEBUG VIS', nodes, edges);
	
	data = { nodes: new vis.DataSet(nodes), edges: new vis.DataSet(edges) };
	
	return data;
}; // }}}


/*
Run the macro
*/
exports.run = function(ident) {
	var currentTid, visbox, nodes = [], edges = [], data, options, network,
		metasTid, metas, formsList, kaiInfo = {}, kaiTid,
		i, j;
	
	var testData = {}, testForms = [];
	
	try {
		
		ident = ident.replace(/(?:\$:\/asekai\/modules\/strains\/|k_)?([a-z0-9]+)(?:\/meta)?/, '$1');
		ident = util.identStrain(ident);
		
		metasTid = $tw.wiki.getTextReference('k_' + ident + '/meta', '');
		metas = JSON.parse(metasTid);
		formsList = $tw.wiki.filterTiddlers('[all[tiddlers+shadows]prefix[k_' + ident + '_]removeprefix[k_' + ident + '_]]');
		
		for (i = 0; i < formsList.length; i++) {
			if (/\/[a-z]+/.test(formsList[i])) {
				formsList.splice(i, 1);
				i--;
				continue;
			}
			
			kaiTid = $tw.wiki.getTiddler( 'k_' + ident + '_' + formsList[i] ).fields;
			kaiInfo[ formsList[i] ] = {};
			
			for (j in kaiTid) {
				kaiInfo[ formsList[i] ][j] = kaiTid[j];
			}
			
			kaiInfo[ formsList[i] ].kerv = ( kaiInfo[ formsList[i] ]['stage-kerv'] === "true" ? true : false );
			kaiInfo[ formsList[i] ].name = kaiLangString(ident + '_' + formsList[i], 'name', '${post}', true);
			//console.log('kailangstring "', kaiInfo[ name ], '"', ident + '_' + formsList[i]);
		}
		
		//onsole.log('METAS TID TEST', ident, metas, kaiInfo);
		
		currentTid = this.getVariable("currentTiddler");
		visbox = document.querySelector('[data-tiddler-title="' + currentTid + '"] .vis');
		
		// create datasets from test data
		data = visData(formsList, metas, kaiInfo);
		
		options = {
			nodes: {
				heightConstraint: {}, widthConstraint: { minimum: 30 }
			},
			edges: {
			},
			physics: {
				repulsion: {
					nodeDistance: 200
				}
			},
			layout: {
				randomSeed: 2
			}
		};
		
		console.log('visdata', currentTid, visbox, data);
		network = new vis.Network(visbox, data, options);
		
		/*network.on("stabilizationIterationsDone", function () {
			network.setOptions( { physics: false } );
		});*/
		
		
	}
	catch (e) { console.error('VIS TEST ERROR', e); }
	return '';
};

})();