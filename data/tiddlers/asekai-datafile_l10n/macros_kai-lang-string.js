/*\
jedit: :folding=explicit: {{{
created: 20190626201846566
tags: $:/asekai/datafile/macros
title: $:/asekai/datafile/macros/kai-lang-string
type: application/javascript
module-type: macro

lang string

\*/
(function(){
/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var heart = require('$:/asekai/datafile/lib/heart.js');

/* Information about this macro */
exports.name = "kai-lang-string";
exports.params = [ {name: "ident"}, {name: "field"}, {name: "sector"}, {name: "plaintext"}, {name: "template"} ];
// }}}

/*
Run the macro
*/
exports.run = function(ident, field, sector, plainText, templateString) {
	var strain, form, identFull, name, post, variant, tiddler, result, language,
	    realField, langDir, separator,
	    configTid, langTid, titleTest,
	    error = 'Error';
	    
	const wiki = (this === undefined ? $tw.wiki : this.wiki),
	      defaultLang = {
			'name-space': ' ', 'name-order': 'an',
			'default-lang': true
		  },
	
	tidName = function(identScoped) {
		var tiddler;
		
		if (identScoped === undefined) {
			identScoped = identFull;
		}
		
		if ( /^((strain|form)-summary|phenoclass|tw-ui|sectors?|strain-devnotes?)$/.test(field) ) {
			tiddler = '$:/asekai/lang/' + language + '/' + langDir + '/' + ident;
			
			if (/^(strain|form)-summary$/.test(field)) {
				tiddler += ('/' + sector);
			}
		}
		//if (field == "name-mean" || field == "name") {
		else {
			tiddler = '$:/asekai/lang/' + language + '/' + langDir + '/' + identScoped;
		}
		
		return tiddler;
	},
	
	variantPron = function() {
		var kaiData, variantName, variant = '';
		
		kaiData = wiki.getTiddler('k_' + identFull);
		langDir = 'variants';
		variantName = wiki.getTiddler(tidName(kaiData.fields.variant));
		
		//onsole.log('name pron debug', kaiData, variantName, tidName(kaiData.fields.variant));
		
		if (kaiData.fields.variant === undefined || variantName === undefined ||
			tiddler['name-variant'] === undefined || tiddler['name-variant'] !== variantName.fields.name) {
			return '';
		}
		
		variant = variantName.fields['name-pron'] + langTid['pron-space'];
		return variant;
	};
	
	
	var nameLength = 'none';
	
	// name-chart is special
	if (field === 'name-chart') {
		// LATER: due to different char sizes, set this limit by language.
		nameLength = 14;
		field = 'name';
		plainText = true;
	}
	
	
	/* determine language parameters & ident {{{ */
	
	if (/^(sort-name|variant-name|tw-ui|sectors?)$/.test(field)) {
		identFull = ident;
	}
	else {
		ident = ident.replace(/^(\$:\/asekai\/lang\/[a-z-]+\/strains\/)?(k_)?/, '');
	
		if (/_/.test(ident) === true) {
			identFull = ident;
			strain = ident.split('_');
			form = strain[1];
			strain = strain[0];
		}
		else if (ident === '') {
			return error;
		}
		else {
			strain = ident;
			form = ident;
			identFull = strain + '_' + form;
		}
	}

	// get current language
	language = wiki.getTextReference('$:/asekai/config/current-language', 'en');
	// get language info such as space
	langTid = wiki.getTextReference('$:/asekai/lang/' + language + '/language', null);
	
	try {
		if (langTid === null) {
			throw ('$:/asekai/lang/' + language + '/language is null');
		}
	
		langTid = JSON.parse(langTid);
	}
	catch (e) {
		console.error('kai lang string error:', e);
		langTid = defaultLang;
	}
	
	/* }}} */
	
	//onsole.log('lang string info', configTid, langTid, language);
	const fieldInfo = {
		dir: {
			"sort-name": 'sorts',
			"variant-name": 'variants',
			"strain-summary": 'strain-summary',
			"form-summary": 'form-summary',
			"tw-ui": 'tw-ui',
			"sector": 'sectors',
			"strain-devnote": 'strain-devnote'
		},
		field: {
			"sort-name": 'name',
			"variant-name": 'name',
			"strain-summary": 'text',
			"form-summary": 'text',
			"tw-ui": 'text',
			"sector": 'name',
			"strain-devnote": 'text'
		},
		error: {
			"strain-summary": '---',
			"form-summary": '---',
			"name-pron": '---',
			"strain-devnote": '---'
		}
	};
	
	
	langDir = fieldInfo.dir[field];
	realField = fieldInfo.field[field];
	error = (fieldInfo.error[field] === undefined ? 'Error' : fieldInfo.error[field]);
	
	if (langDir === undefined) {
		langDir = 'strains';
	}
	if (realField === undefined) {
		realField = field;
	}
	

	tiddler = wiki.getTiddler(tidName());
	tiddler = heart.flattenAttributes(tiddler);
	
	// debug
	//onsole.log('lang string debug', field, ident, sector, tiddler); return tidName();
		
	// if string not found, use the english one
	if (tiddler === undefined || tiddler.title === undefined) {
		language = 'en';
		tiddler = wiki.getTiddler(tidName());
		tiddler = heart.flattenAttributes(tiddler);
		
		langTid = defaultLang;
	}
	// in the bizarre event the lang tid isn't defined
	// but the name can be found, don't italicise the name
	else {
		langTid['default-lang'] = false;
	}
	
	
	//onsole.log('LANG STRING DEBUG', tidName(), tiddler);
	//onsole.log(ident, field, templateString, plainText);
	
	try {
	
	if (field == 'tiddler') {
		return tiddler;
	}
	if (field == 'name') {
		name = tiddler.name;
		post = tiddler['name-post'];
		var postShort = tiddler['name-postshort'];
		variant = tiddler['name-variant'];
		separator = tiddler['name-space'];
		
		if (name === '' || name === undefined) {
			name = ident;
		}
		else {
			if (variant !== undefined && variant !== '') {
				if (separator === undefined) {
					separator = langTid['name-space'];
				}
				
				// if name is considered "too long", abbreviate variant
				// this orders name like "Millennium Sokukou" because it doesn't matter
				if ( nameLength * 1 > 0 && (name + separator + variant).length > nameLength ) {
					// crudely chop variant to first letter
					// LATER: do this a little more fancily, set period etc by language
					variant = variant[0] + '.';
				}
				
				// put variant first or last depending on language grammar
				// - romance languages etc may order names like "Nixdent Violeta"
				if (langTid['name-order'] == 'na') {
					name = name + separator + variant;
				}
				else {
					name = variant + separator + name;
				}
			}
			
			if (post !== undefined && post !== '') {
				if (typeof templateString !== 'string' || templateString == '') {
					templateString = '{name} - {post}';
				}
				
				name = templateString.replace('{name}', name).replace('{post}', post).replace('{postshort}', postShort);
			}
		}
		
		// if plaintext, return raw name
		// else return formatting
		if (plainText !== true) {
			// prevent wiki links
			/*if (/[a-z][A-Z]/.test(name)) {
				name = '~' + name;
			}*/
			
			// convert 'lf' characters to soft hyphens
			name = name.replace(/␊/g, '&shy;');
			
			if (langTid['default-lang'] === true) {
				name = '<i>' + name + '</i>';
			}
		}
		else {
			// strip soft hyphens
			name = name.replace(/␊/g, '');
		}

		return name;
	}
	// used both for IPA-using language pron keys,
	// and japanese pronunciations
	else if (field == 'variant-pron') {
		return variantPron();
	}
	// mainly used for japanese
	// for non-phonetic languages, call kai-name-pron
	else if (field == 'name-pron') {
		if (tiddler.name === undefined) {
			return error;
		}
		
		variant = variantPron();
		
		// if pronunciation is undefined just plop the name in
		// LATER: modify this to do something a little more sensible
		// for languages that aren't japanese.
		
		if (tiddler['name-pron'] !== undefined) {
			result = variant + tiddler['name-pron'];
		}
		else {
			result = variant + tiddler['name'];
		}
		
		// in this case error is also just for 'none needed'
		if (result == (tiddler['name-variant'] + langTid['name-space'] + tiddler['name']) || result == tiddler['name']) {
			return error;
		}
		
		return result;
	}
	else {
		if (tiddler.title === undefined) {
			throw "Tiddler empty";
			
		}
		
		result = tiddler[realField];
		return result;
	}
	
	}
	catch (e) {
		//onsole.log('Problem with lang string', field, ident, tidName(), tiddler);
		return error;
	}
};

})();