#!/bin/bash

mode=$1

main() {
  if [ "$mode" = "clean" ]; then
    cleanstate
  else
    cleandata;  tiddlywiki
  fi
}

tiddlywiki() {
  ./node_modules/tiddlywiki/tiddlywiki.js ./ --listen port=8090
}

cleanstate() {
if ls ./tiddlers/Draft* 1> /dev/null 2>&1; then
  echo "removing ./Drafts";  ls ./tiddlers/Draft*;  rm ./tiddlers/Draft*
fi
if ls ./tiddlers/tw-state/Draft* 1> /dev/null 2>&1; then
  echo "removing tw-state/Drafts";  ls ./tiddlers/tw-state/Draft*;  rm ./tiddlers/tw-state/Draft*
fi
}
cleandata() {
if [[ -f ./data_output/tiddlers/Asekai_*.tid ]]; then
  echo "removing plugins";  ls ./data_output/tiddlers/Asekai_*.tid;  rm ./data_output/tiddlers/Asekai_*.tid
fi
}

main
