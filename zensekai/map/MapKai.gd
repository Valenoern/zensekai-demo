extends StaticBody
class_name MapKai
# :folding=explicit: <- jedit prefs

var ident  # kai strain/form
var meshData; var collisionData  # non-instantiated data
var model; var collision; var shapeOwner  # instantiated models
var scene
var materialised = false

# godot collisions are handled by putting PhysicsBodies in layers
# collision_layer identifies the object, collision_mask what it will hit
# this project's layers:
# 8    4      2     1
# none notice agent player

# map kai accept click events,
# but these are not acknowledged yet
# they are also finicky, but that's the fault of godot's 'input ray' system
func _input_event(camera, event, click_positions, click_normal, shape_idx):
	#print("click positions", event.is_action_pressed("ui_click"))
	
	if event.is_action_pressed("ui_click") == true:
		print("Click")
#

# show MapKai when cylinder enters Leader's NoticeRadius
# called from NoticeRadius rather than an event here
# TODO: change this to be an event to get rid of mapkailair fetch shenanigans
func leader_noticed():
	#print("collision")
	materialise()
#

# show model
# displaying model is separated from constructor so it can be unloaded sometimes
func materialise():
	# global scene
	if materialised == false:
		materialised = true
		#self.model.visible = true
		scene.visible = true
		disable_notice()

# unload model data
func unload():
	if materialised == true:
		materialised = false
		disable_notice()
		#remove_child(self.model)
		#self.model = null
		# self.collision.free()
		#remove_child(self.collision)
		#self.collision = null

# (re)load model data
func reload():
	add_mesh()
	add_collision()
	enable_notice()
#

# add mesh as node
func add_mesh(): # {{{
	pass
	# i at first thought the only thing to set on a MeshInstance
	# was the mesh inside, but there are many other properties
	# so it's best to duplicate the whole MeshInstance
	# as that *will not* duplicate the kai mesh polygons themselves
	
	#var mesh = self.meshData
	#add_child(mesh)
	#self.model = mesh
# }}}

# add collision shape as node
func add_collision(): # {{{
	pass
	# global collisionData, collision, shapeOwner
	#var model = self.model
	
	# 'formal' CollisionShape is not stored with reference model
	# so make a new one
	#var collisionHolder = CollisionShape.new()
	#collision = collisionHolder
	#model.add_child(collisionHolder)
	#model.add_child(collisionData)
	
	# attempt to use the collision data specified in _init
	#collisionHolder.make_convex_from_brothers()
	#var collisionPoints = collisionHolder.get_shape()
	#shape_owner_add_shape(shapeOwner, collisionPoints)
	
	#print("shape owner", get_shape_owners())
	
	#remove_child(collisionData)
	
	#print("ADDED COLLISION", collisionData, collisionHolder.get_shape())
# }}}

static func default_collisionshape(): # {{{
	var cylinder = CylinderShape.new()
	cylinder.set_height(2)
	cylinder.set_radius(1)
	return cylinder
# }}}

# enable HitRadius (player) and NoticeRadius collisions
func enable_notice(): # / disable_notice() {{{
	set_collision_layer(2) #0010 - is agent
	set_collision_mask(5) #0101 - seeks player, notice

# disable NoticeRadius collisions
func disable_notice():
	set_collision_layer(8) #1000 - is nothing
	set_collision_mask(1) #0001 - seeks player
# }}}

# called when entering tree
func _ready():
	reload()
#

# constructor; takes a kai model
func _init(modelRef):
	self.ident = modelRef.ident
	#var mesh = modelRef.mesh
	#var collision = modelRef.collision
	
	#if is_instance_valid(mesh) == false:
		#print("INVALID MESH. modelref:", modelRef)
	
	# if collision is empty, use default
	#if is_instance_valid(collision) == false:
		#collision = default_collisionshape()
	
	#self.meshData = mesh.get_mesh()
	#self.meshData = mesh
	#self.collisionData = collision
	#self.shapeOwner = create_shape_owner(self)
	
	self.scene = modelRef.scene
	add_child(modelRef.scene)
#
