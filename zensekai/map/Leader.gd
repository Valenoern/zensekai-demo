extends KinematicBody

#onready var leaderCam = $ARVROrigin/LeaderCam
onready var noticeRadius = $NoticeRadius
var velocity = Vector3()
var walkSpeed = 0.2
var maxSpeed = 0.7
var slowSpeed = walkSpeed * 0.05

# at first i got very confused and thought
# you were supposed to put player characters inside the ARVROrigin
# but no. despite it being called an origin...


# when scenery is called back to origin by zeroing_sabre (MapKaiLair)
# snap leader there
func _on_scenery_zeroed():
	print("ZEROING SABRE - leader returned to origin")
	translate(Vector3(0, 0, 0))
#

# for MapKai entering NoticeRadius
func _on_body_entered(body):
	# collisions return a PhysicsBody, not the MapKai the object is......
	# so we have to do some absolute shenanigans to retrieve them
	var mapkai = get_parent().get_node("MapKaiLair").get_position(body.name)
	
	# for a bit i had a problem with HitRadius colliding with NoticeRadius,
	# so added this check for "null instance" errors.
	# they don't happen after fixing that but i'm leaving it
	if is_instance_valid(mapkai):
		#print(mapkai)
		mapkai.leader_noticed()
#

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# apply slow before speeding up
	slow()
	
	# add input to velocity vector which is applied each delta
	# as it keeps adding, make sure it doesn't exceed a reasonable speed!
	if Input.is_action_pressed("ui_left") and velocity.x > -1 * maxSpeed:
		velocity.x = velocity.x - walkSpeed
	elif Input.is_action_pressed("ui_right") and velocity.x <= 1 * maxSpeed:
		velocity.x = velocity.x + walkSpeed
	if Input.is_action_pressed("ui_up") and velocity.z > -1 * maxSpeed:
		velocity.z = velocity.z - walkSpeed
	elif Input.is_action_pressed("ui_down") and velocity.z <= 1 * maxSpeed:
		velocity.z = velocity.z + walkSpeed
	#
	
	# apply velocity vector to move leader
	# when you use "KinematicBody.move_and_collide", rather than "node.translate",
	# godot will automatically stop a PhysicsBody from clipping
	move_and_collide(velocity)
#

# when not getting input,
# slow down gradually by 'dulling' velocity vector
func slow():
	var signX = sign(velocity.x)
	var signZ = sign(velocity.z)
	
	velocity.x = abs(velocity.x) - slowSpeed
	if velocity.x < 0: velocity.x = 0.0
	velocity.x = velocity.x * signX
	
	velocity.z = abs(velocity.z) - slowSpeed
	if velocity.z < 0: velocity.z = 0.0
	velocity.z = velocity.z * signZ
#

# Called when the node enters the scene tree for the first time.
func _ready():
	noticeRadius.connect("body_entered", self, "_on_body_entered")
#
