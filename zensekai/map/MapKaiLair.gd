extends Spatial
# script containing 'library functions'
const maplib = preload("map.gd")
# :folding=explicit: <- jedit prefs

onready var refreshTimer = $refreshTimer
onready var models = $Models
onready var grids = $GridGrid
var positions

var numGrids = 0
var currentCell # used for laying down new grids

signal zeroing_sabre  # calls everything back to origin


func _leader_exited_grid(body, offset): # {{{
	var translation = body.get_translation()
	var difference = translation - offset
	# copy current cell vector in order to do arithmetic on it
	var newOrigin = Vector3(currentCell.x, currentCell.y, currentCell.z)
	print("you left ", grid_at(currentCell), " at ", difference)
	
	# dematerialise this grid
	positions.unload_kai()
	positions.disable_leave()
	
	# try to determine which side was left
	# by finding the translation from the grid origin
	if difference.z <= -100:
		newOrigin.z = currentCell.z - 1
	elif difference.z >= 100:
		newOrigin.z = currentCell.z + 1
	# "if" allows leaving two sides at once on a corner
	if difference.x <= -100:
		newOrigin.x = currentCell.x - 1
	elif difference.x >= 100:
		newOrigin.x = currentCell.x + 1
	
	currentCell = newOrigin
	
	# attempt to create new grid
	var gridAtNew = get_node_or_null(grid_at(newOrigin))
	print("GRIDATNEW", gridAtNew)
	
	if is_instance_valid(gridAtNew) == true:
		positions = gridAtNew
	else:
		positions = new_grid(newOrigin)
	
	# re-enable new grid
	positions.enable_leave()
	positions.reload_kai()
	print("you are now in: ", grid_at(newOrigin))
# }}}

# called when refresh timer goes off
func _on_timer_refresh(): # {{{
	var grid
	
	print("refresh timer went off")
	
	# flush old grids
	var children = grids.get_children()
	for grid in children:
		grid.free()
	numGrids = 0
	
	# TODO: maybe spare kai inside NoticeRadius?
	
	# snap the leader back to the virtual zero point
	# LATER: recalibrate all the scenery to there
	emit_signal("zeroing_sabre")
	currentCell = maplib.zero_vector()
	
	# generate new grid
	positions = new_grid(currentCell)
# }}}

# add new grid to "GridGrid"
# should be called as positions = newGrid()
func new_grid(origin):
	numGrids += 1
	
	# put this grid in a new node
	var gridNode = MapKaiGrid.new(origin, models)
	gridNode.set_name(grid_at(currentCell))
	grids.add_child(gridNode)
	
	# detect exiting of grid and handle it in this script
	gridNode.connect("leader_exited_grid",self,"_leader_exited_grid")
	return gridNode
#

# retrieve a MapKai from current grid - unused?
func get_position(name):
	return positions.get_node(name)
#

func _ready():
	positions = $GridGrid
	models.hide()
	
	currentCell = maplib.zero_vector()
	positions = new_grid(currentCell)
	
	# setup grid refreshing
	refreshTimer.connect("timeout",self,"_on_timer_refresh") 
	refreshTimer.start()
#


# maplib imports {{{

# convert the integer coordinates of a grid to direction letters
func grid_at(vector3d):
	return maplib.grid_at(vector3d)

# for debug; just a funnily named nop function
func dont():
	pass

# }}}