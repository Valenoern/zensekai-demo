#!/bin/bash

# grep -o returns only the matching string
platform=`uname -a | grep -o "^\(Linux\|MSYS_NT\)"`

echo "$platform"
