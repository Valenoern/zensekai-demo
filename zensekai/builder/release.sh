# package codeberg release

cd "${0%/*}"; cd ../..

sourcedir=`pwd`
day=`date +"%Y-%m-%d"`
filename="${day}_$1.tar.gz"
tarrable="ACKNOWLEDGEMENTS  INTENT  LICENSE  README.md  data docs guide zensekai*"

if [[ -f "/tmp/$filename" ]]; then
 rm "/tmp/$filename"
 rm -rf "/tmp/$filename.d"
fi

mkdir "/tmp/$filename.d"
cp -a $tarrable "/tmp/$filename.d/"
cd "/tmp/$filename.d"
mv ./zensekai ./zensekai-src
mv ./zensekai-bin ./zensekai

# cleanup build artifacts
rm -rf ./zensekai-src/bin
rm -rf ./zensekai-src/wit/target

tar -czvf "../$filename" $tarrable
